package game

import (
	"fmt"
)

type NameSeq struct {
	Names           []string
	Len             int
	Pos             int
	Postfix         string
	OverflowCounter int
}

func (seq *NameSeq) Next() string {
	var name = fmt.Sprintf("%v%v", seq.Names[seq.Pos], seq.Postfix)
	seq.Pos += 1
	if seq.Pos >= seq.Len {
		seq.OverflowCounter += 1
		seq.Pos = 0
		seq.Postfix = fmt.Sprintf("%v", seq.OverflowCounter)
	}
	return name
}

var PlayerNameSeq = NameSeq{
	Names: []string{
		"Федя", "Саша", "Костик",
		"Антоша", "Сережа", "Тимурка",
		"Илюшка", "Мишутка",
	},
	Len:             8,
	Pos:             0,
	Postfix:         "",
	OverflowCounter: 0,
}
