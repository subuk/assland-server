package game

import (
	"github.com/robfig/revel"
)

const (
	EVENT_INIT              = 0
	EVENT_MOVE              = 1
	EVENT_NEW_PLAYER_JOINED = 2
	REQ_JOIN                = 3
	EVENT_JOIN_ACCEPT       = 4
	REQ_MOVE                = 5
	EVENT_OBJ_MOVED         = 6
	EVENT_OBJ_REMOVED       = 7
)

type Event struct {
	Type int         `json:"type"`
	Body interface{} `json:"body"`
}

type Position struct {
	X int `json:"x"`
	Y int `json:"y"`
}

type Object struct {
	Name string   `json:"name"`
	Type string   `json:"type"`
	Pos  Position `json:"pos"`
}

type ObjectCollection struct {
	Objects []Object `json:"objects"`
}

type Game struct {
	ObjectCollection
	Id       int          `json:"id"`
	Channels []chan Event `json:"-"`
}

func (self *ObjectCollection) Add(obj Object) {
	self.Objects = append(self.Objects, obj)
}

var CurrentGame = Game{
	Id: 10,
	ObjectCollection: ObjectCollection{
		Objects: dummyObjects,
	},
	Channels: []chan Event{},
}

func (g *Game) Join(name string, x int, y int) (chan Event, Object) {
	name = PlayerNameSeq.Next()
	player := Object{
		Name: name,
		Type: "Player",
		Pos:  Position{X: x, Y: y},
	}
	g.Add(player)
	g.Publish(Event{
		Type: EVENT_NEW_PLAYER_JOINED,
		Body: player,
	})
	var ch = make(chan Event, 100)
	g.Channels = append(g.Channels, ch)
	revel.INFO.Printf(">>> Clients number: %d.", len(g.Channels))
	return ch, player
}

func (g *Game) Cancel(toCancel chan Event, player Object) {
	revel.INFO.Printf("Cancelling subscription")

	// remove channel
	var newChannels = []chan Event{}
	for _, ch := range g.Channels {
		if ch != toCancel {
			newChannels = append(newChannels, ch)
		}
	}
	g.Channels = newChannels

	// remove player object
	revel.INFO.Printf(">>> Objects count: %v", len(g.Objects))
	var newObjects = []Object{}
	for _, obj := range g.ObjectCollection.Objects {
		if obj.Name != player.Name {
			newObjects = append(newObjects, obj)
		}
	}
	g.ObjectCollection.Objects = newObjects
	revel.INFO.Printf(">>> Objects count: %v", len(g.Objects))

	g.Publish(Event{
		Type: EVENT_OBJ_REMOVED,
		Body: player,
	})

}

func (g *Game) Publish(event Event) {
	for _, ch := range g.Channels {
		revel.INFO.Printf(">>> Publishing event %v to channel %v", event, ch)
		ch <- event
		revel.INFO.Printf(">>> OK")
	}
}

func (g *Game) SetObjectPosition(name string, newPos Position) {
	var movedObj *Object = nil
	for i, _ := range g.Objects {
		var obj = &g.Objects[i]
		if obj.Name == name {
			obj.Pos.X = newPos.X
			obj.Pos.Y = newPos.Y
			movedObj = obj
			break
		}
	}
	if movedObj == nil {
		revel.WARN.Printf("Cannot set object position. Object %s not found", name)
	} else {
		g.Publish(Event{
			Type: EVENT_OBJ_MOVED,
			Body: movedObj,
		})
	}
}
