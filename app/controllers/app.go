package controllers

import (
	"assland-server/app/game"
	"code.google.com/p/go.net/websocket"
	"github.com/robfig/revel"
)

type App struct {
	*revel.Controller
}

func (self App) Index() revel.Result {
	return self.Render()
}

func (self App) Stream(ws *websocket.Conn) revel.Result {
	defer ws.Close()
	var message = game.Event{
		Type: game.EVENT_INIT,
		Body: game.CurrentGame.ObjectCollection,
	}
	revel.INFO.Printf("New client connected. Sending intial game info. Object count %v", len(game.CurrentGame.ObjectCollection.Objects))
	websocket.JSON.Send(ws, message)

	wsRequests := make(chan game.Event, 5)
	go func() {
		var msg = game.Event{}
		for {
			err := websocket.JSON.Receive(ws, &msg)
			if err != nil {
				close(wsRequests)
				return
			}
			wsRequests <- msg
		}
	}()

	// Waiting for join
	revel.INFO.Println("Waiting for join request")
	req, ok := <-wsRequests
	if !ok {
		revel.INFO.Println("Client closed connection")
		return nil
	}
	if req.Type != game.REQ_JOIN {
		revel.WARN.Printf(
			"Protocol error. Client sends request of type %d instead of %d",
			req.Type, game.REQ_JOIN)
		return nil
	}

	// Joining
	revel.INFO.Println("Got join request from client")
	revel.INFO.Println("Joining client to game")
	var gameChannel, currentPlayer = game.CurrentGame.Join("", 30*16, 15*16)
	defer game.CurrentGame.Cancel(gameChannel, currentPlayer)

	revel.INFO.Println("Sending accept to new player %v", currentPlayer.Name)
	websocket.JSON.Send(ws, game.Event{
		Type: game.EVENT_JOIN_ACCEPT,
		Body: currentPlayer,
	})

	revel.INFO.Println("Handling events for this client")
	for {
		select {

		// Publishing game events to client
		case event := <-gameChannel:
			revel.INFO.Println("New event")
			if event.Type == game.EVENT_NEW_PLAYER_JOINED {
				revel.INFO.Println("Publishing new player joined event")
				websocket.JSON.Send(ws, event)
			}
			if event.Type == game.EVENT_OBJ_MOVED {
				revel.INFO.Println("Publishing object moved event")
				websocket.JSON.Send(ws, event)
			}
			if event.Type == game.EVENT_OBJ_REMOVED {
				revel.INFO.Println("Publishing object removed event")
				websocket.JSON.Send(ws, event)
			}

		// Receiving requests from client
		case req, ok := <-wsRequests:
			revel.INFO.Println("New request")
			if !ok {
				revel.INFO.Println("Client closed connection")
				return nil
			}
			revel.INFO.Printf("Processing request with type id %d", req.Type)
			if req.Type == game.REQ_JOIN {
				revel.WARN.Printf("Player %s sent second join request in one session")
			}
			if req.Type == game.REQ_MOVE {
				revel.INFO.Printf("Move request received")
				var body = req.Body.(map[string]interface{})
				var name = body["name"].(string)
				var bodyPos = body["pos"].(map[string]interface{})
				var pos = game.Position{
					X: int(bodyPos["x"].(float64)),
					Y: int(bodyPos["y"].(float64)),
				}
				game.CurrentGame.SetObjectPosition(name, pos)
			}
		}
	}
	revel.INFO.Println("Closing connection")
	return nil
}
