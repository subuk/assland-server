SHELL := /bin/bash
GOPATH := ~/gocode
PATH := $(PATH):$(GOPATH)/bin
APPNAME := assland-server
DEPLOY_HOST := assland@assland.openpz.org

package:
	export GOPATH=$(GOPATH); revel package $(APPNAME)

deploy: package
	scp $(APPNAME).tar.gz $(DEPLOY_HOST):~/
	ssh $(DEPLOY_HOST) tar xf $(APPNAME).tar.gz
	ssh $(DEPLOY_HOST) chmod +x ./run.sh
